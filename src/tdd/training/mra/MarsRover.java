package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private int[][] grid;
	private ArrayList<String> planetObstacles;
	private int planetX;
	private int planetY;
	private char direction;
	private int roverX;
	private int roverY;
	private String obstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		grid = new int[planetX][planetY];
		this.planetX = planetX;
		this.planetY = planetY;
		obstacles = "";
		
		
		this.planetObstacles = new ArrayList<>(planetObstacles);
		
		for(int i=0; i<this.planetObstacles.size(); i++) {
			String[] parts = planetObstacles.get(i).substring(1, planetObstacles.get(i).length() -1).split(",");
			int x = Integer.parseInt(parts[0]);
			int y = Integer.parseInt(parts[1]);
			
			this.grid[x][y] = 1;
		}
		
		roverX = 0;
		roverY = 0;
		direction = 'N';
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return grid[x][y] == 1 ? true : false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString != "") {
			for(int i=0; i<commandString.length(); i++) {
				
				switch(commandString.charAt(i)) {
					case 'r':
						turnRight();
						break;
					case 'l':
						turnLeft();
						break;
					case 'f':
						forward();
						break;
					case 'b':
						backward();
						break;
					default:
						break;
				}
			}
						
			return "(" + roverX + "," + roverY + "," + direction + ")" + obstacles;
				
		}
		return "(0,0,N)";
		
	}
	
	private void turnRight() {
		
		switch(direction) {
			case 'N':
				direction = 'E';
				break;	
			case 'E':
				direction = 'S';
				break;	
			case 'S':
				direction = 'W';
				break;	
			case 'W':
				direction = 'N';
				break;
			default:
				break;
		}
	}
	
	private void turnLeft() {
		
		switch(direction) {
			case 'N':
				direction = 'W';
				break;	
			case 'E':
				direction = 'N';
				break;	
			case 'S':
				direction = 'E';
				break;	
			case 'W':
				direction = 'S';
				break;	
			default:
				break;
		}
	}
	
	private void forward() {
		
		int tempY;
		int tempX;

		switch(direction) {
			case 'N':
				tempY = roverY+1;
				if(tempY == planetY) {
					tempY = 0;
					
				}
				checkObstacle(roverX, tempY);
				break;	
			case 'E':
				tempX = roverX+1;
				if(tempX == planetX) {
					tempX = 0;
				}
				checkObstacle(tempX, roverY);
				break;	
			case 'S':	
				tempY = roverY-1;
				if(tempY == -1) {
					tempY = planetY-1;
				}
				checkObstacle(roverX, tempY);
				break;	
			case 'W':
				tempX = roverX-1;
				if(tempX == -1) {
					tempX = planetX-1;
				}
				checkObstacle(tempX, roverY);
				break;
			default:
				break;
		}
	}
	
	private void backward() {
		
		int tempY;
		int tempX;
		
		switch(direction) {
			case 'N':
				tempY = roverY-1;
				if(tempY == -1) {
					tempY = planetY-1;
				}
				checkObstacle(roverX, tempY);
				break;	
			case 'E':
				tempX = roverX-1;
				if(tempX == -1) {
					tempX = planetY-1;
				}
				checkObstacle(tempX, roverY);
				break;	
			case 'S':
				tempY = roverY+1;
				if(tempY == planetY) {
					tempY = 0;
				}
				checkObstacle(roverX, tempY);
				break;	
			case 'W':
				tempX = roverX+1;
				if(tempX == planetX) {
					tempX = planetY-1;
				}
				checkObstacle(tempX, roverY);
				break;	
			default:
				break;
		}
	}
	
	private boolean checkObstacle(int x, int y) {
		if(grid[x][y] == 1) {
			if(!obstacles.contains("(" + x +","+ y + ")")) {
				obstacles = obstacles + "(" + x +","+ y + ")";
			}
			return true;
		} else {
			roverX = x;
			roverY = y;
			return false;
		}
	}

}
