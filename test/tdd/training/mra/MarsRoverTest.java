package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void obstacleAt4_7() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void obstacleAt2_3() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(2, 3));
	}
	
	@Test
	public void notObstacleAt4_4() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(6,7)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertFalse(marsRover.planetContainsObstacleAt(4, 4));
	}
	
	@Test
	public void roverStatusShoulBe0_0_N() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)", marsRover.executeCommand(""));
	}
	
	@Test
	public void roverAt0_0DirectionShouldBeEast() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();

		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)", marsRover.executeCommand("r"));
	}
	
	@Test
	public void roverAt0_0DirectionShouldBeWest() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,W)", marsRover.executeCommand("l"));
	}
	
	@Test
	public void roverStatusShouldBe0_1_N() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,1,N)", marsRover.executeCommand("f"));
	}
	
	@Test
	public void roverStatusShouldBe0_0_N() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		marsRover.executeCommand("f");
		
		assertEquals("(0,0,N)", marsRover.executeCommand("b"));
	}
	
	@Test
	public void roverStatusShouldBe2_2_E() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(2,2,E)", marsRover.executeCommand("ffrff"));
	}
	
	@Test
	public void roverStatusShouldBe3_4_W() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(3,4,W)", marsRover.executeCommand("rffflffffl"));
	}
	
	@Test
	public void roverStatusShouldBe0_9_N() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,9,N)", marsRover.executeCommand("b"));
	}
	
	@Test
	public void roverStatusShouldBe0_9_W() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(9,0,W)", marsRover.executeCommand("lf"));
	}
	
	@Test
	public void roverStatusShouldBe1_2_EEAndObtacle2_2() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)", marsRover.executeCommand("ffrfff"));
	}
	
	@Test
	public void roverStatusShouldBe1_1_EEObtacles2_2And2_1() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,1,E)(2,2)(2,1)", marsRover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	public void roverStatusShouldBe0_0_NObtacles0_9() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)(0,9)", marsRover.executeCommand("b"));
	}
	
	@Test
	public void roverStatusShouldBe3_1_NObstacles3_0And1_2() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(3,0)");
		planetObstacles.add("(1,2)");
		MarsRover marsRover = new MarsRover(4, 4, planetObstacles);
		
		assertEquals("(3,1,N)(3,0)(1,2)", marsRover.executeCommand("lfffrrflllbblffl"));
	}
	
	@Test
	public void roverStatusShouldBe0_0_NObtacles2_2And0_5And5_0() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover marsRover = new MarsRover(6, 6, planetObstacles);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", marsRover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
	
	
	

}
